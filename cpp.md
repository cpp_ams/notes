# C++

---

1. Preprocessor runs
2. Compilation: cpp files are compiled into .o files
3. Link: combine files into executable

##### Static libs:
- compiled into own program

##### Dynamic libs:
- program finds relevant code at runtime


---

### [Difference between #include <filename> and #include “filename”?](https://stackoverflow.com/questions/21593/what-is-the-difference-between-include-filename-and-include-filename)

---

### [Difference between vector and list in stl?](https://stackoverflow.com/questions/2209224/vector-vs-list-in-stl)

---

###  RAII (Resource Allocation Is Initialization)
```c++
class AnimalPtr {
  Animal *m_animal;
public:
  AnimalPtr() {
   m_animal = new Animal;
  }
  ~AnimalPtr() {
    delete m_animal;
  }
  setName(string name) {
    m_animal->setName(name);
  }
  // ...
};
```
##### use it like this
```c++
AnimalPtr cat1;  // Calls new
cat1.setName("freddy");
// ...
// delete will be called when cat1 goes out of scope
```
---
### Init difference

##### Animal animal
Uses memory from stack. Gets destroyed when out of scope.
##### Animal *pointer = new Animal
This uses memory from the heap. Memory needs to be allocated and released. Needs do be deleted.

---

### [const usage in c++](https://stackoverflow.com/questions/5598703/c-const-usage-explanation)

---

### Writing operation overloading inside or outside of class

If it takes more than one argument, it should be declared outside of the class:

Example:
```c++
cout << x;
is interpreted by the compiler as
operator <<(cout, x);
1.0 + b // The compiler will try to call (1.0).operator+(b)

Inside class:
b == 1.0; // This calls b.operator==(1.0)
1.0 + b // The compiler will try to call (1.0).operator+(b) ERROR!!
```

---

### [Functors in  c++](https://stackoverflow.com/questions/356950/what-are-c-functors-and-their-uses)

---

### [When to use which cast](https://stackoverflow.com/questions/332030/when-should-static-cast-dynamic-cast-const-cast-and-reinterpret-cast-be-used)

---

### [unique and shared ptr](https://stackoverflow.com/questions/6876751/differences-between-unique-ptr-and-shared-ptr)

---

### [pragma pack](https://stackoverflow.com/questions/3318410/pragma-pack-effect)

---

### [List initialisation](https://stackoverflow.com/questions/18222926/why-is-list-initialization-using-curly-braces-better-than-the-alternatives)

---

### ['printf' vs. 'cout' in C++](https://stackoverflow.com/questions/2872543/printf-vs-cout-in-c)

---

string function .back(), .front()


